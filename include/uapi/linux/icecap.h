#pragma once

#include <linux/ioctl.h>

#define ICECAP_IOCTL_MAGIC 0x33
#define ICECAP_VMM_PASSTHRU _IOWR(ICECAP_IOCTL_MAGIC, 0, struct icecap_vmm_passthru)
#define ICECAP_RESOURCE_SERVER_YIELD_TO _IOWR(ICECAP_IOCTL_MAGIC, 1, struct icecap_resource_server_yield_to)

struct icecap_vmm_passthru {
    unsigned long sys_id;
    unsigned long regs[7];
};

struct icecap_resource_server_yield_to {
    unsigned long realm_id;
    unsigned long virtual_node;
};
