#define DEBUG

#include <linux/console.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/list.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>
#include <linux/serial_core.h>
#include <linux/stringify.h>
#include <linux/types.h>

#include "../../../drivers/tty/hvc/hvc_console.h"

#include "ring_buffer.h"

static icecap_ring_buffer_t rb = {0};

static int get_chars(uint32_t vtermno, char *buf, int count)
{
	int real_count = min(count, /* HACK */ (int)icecap_ring_buffer_poll_read(&rb));
	if (real_count > 0) {
		icecap_ring_buffer_read(&rb, real_count, buf);
		icecap_ring_buffer_notify_read(&rb);
	}
	icecap_ring_buffer_enable_notify_read(&rb);
	return real_count;
}

static int put_chars(uint32_t vtermno, const char *buf, int count)
{
	int real_count = min(count, /* HACK */ (int)icecap_ring_buffer_poll_write(&rb));
	if (real_count > 0) {
		icecap_ring_buffer_write(&rb, real_count, buf);
		icecap_ring_buffer_notify_write(&rb);
	}
	icecap_ring_buffer_enable_notify_write(&rb);
	return real_count;
}

static const struct hv_ops ops = {
	.get_chars = get_chars,
	.put_chars = put_chars,
	.notifier_add = notifier_add_irq,
	.notifier_del = notifier_del_irq,
	.notifier_hangup = notifier_hangup_irq,
};

static int probe(struct platform_device *dev)
{
	struct hvc_struct *hp;

	printk("icecap-con: probe");

	icecap_ring_buffer_of(dev, &rb);
	icecap_ring_buffer_enable_notify_read(&rb);
	icecap_ring_buffer_enable_notify_write(&rb);

	hp = hvc_alloc(0, 0, &ops, PAGE_SIZE);
	if (IS_ERR(hp))
		return PTR_ERR(hp);
	hp->data = platform_get_irq(dev, 0);
	BUG_ON(hp->data < 0);

	return 0;
}

static int remove(struct platform_device *dev)
{
	printk("icecap-con: remove");
	return 0;
}

static struct of_device_id match_table[] = {
	{
		.compatible = "icecap,con",
	},
	{},
};
MODULE_DEVICE_TABLE(of, match_table);

static struct platform_driver icecap_con = {
	.probe = probe,
	.remove = remove,
	.driver = {
		.name = "icecap_con",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(match_table),
	},
};
module_platform_driver(icecap_con);
