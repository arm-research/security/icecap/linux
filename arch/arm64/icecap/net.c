#define DEBUG

#include <linux/delay.h>
#include <linux/err.h>
#include <linux/errno.h>
#include <linux/etherdevice.h>
#include <linux/in.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ip.h>
#include <linux/irq.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/netdevice.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>
#include <linux/sched.h>
#include <linux/skbuff.h>
#include <linux/slab.h>
#include <linux/stringify.h>
#include <linux/tcp.h>
#include <linux/types.h>

#include "ring_buffer.h"

// #define VERBOSE

#ifdef VERBOSE
#define icecap_log_debug(...) printk(__VA_ARGS__)
#else
#define icecap_log_debug(...)
#endif

static int napi_weight = 128;
module_param(napi_weight, int, 0444);

struct icecap_net_priv {
	icecap_ring_buffer_t rb;
	int irq;
	struct napi_struct napi;
	spinlock_t lock;
	struct net_device *dev;
};

static void icecap_net_enable_rx_cb(struct icecap_net_priv *priv)
{
	icecap_ring_buffer_enable_notify_read(&priv->rb);
}

static void __maybe_unused icecap_net_enable_tx_cb(struct icecap_net_priv *priv)
{
	icecap_ring_buffer_enable_notify_write(&priv->rb);
}

static void icecap_net_disable_rx_cb(struct icecap_net_priv *priv)
{
	icecap_ring_buffer_disable_notify_read(&priv->rb);
}

static void icecap_net_disable_tx_cb(struct icecap_net_priv *priv)
{
	icecap_ring_buffer_disable_notify_write(&priv->rb);
}

static netdev_tx_t icecap_net_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
#ifdef VERBOSE
	int i;
#endif
	int len;
	char *data, shortpkt[ETH_ZLEN];
	struct icecap_net_priv *priv = netdev_priv(dev);

	icecap_log_debug("icecap-net: tx %d", skb->len);
#ifdef VERBOSE
	icecap_log_debug("icecap-net: tx [ ");
	for (i = 0; i < skb->len; i++) {
		icecap_log_debug(KERN_CONT "%02X ", skb->data[i]);
	}
	icecap_log_debug(KERN_CONT "]");
#endif

	if (skb->len < ETH_ZLEN) {
		memset(shortpkt, 0, ETH_ZLEN);
		memcpy(shortpkt, skb->data, skb->len);
		len = ETH_ZLEN;
		data = shortpkt;
	} else {
		len = skb->len;
		data = skb->data;
	}

	if(icecap_ring_buffer_poll_write_packet(&priv->rb, len) == ICECAP_RING_BUFFER_NO_PACKET) {
		BUG();
		return NETDEV_TX_BUSY;
	}

	icecap_ring_buffer_write_packet(&priv->rb, len, data);
	icecap_ring_buffer_notify_write(&priv->rb);

	dev_kfree_skb(skb);
	return NETDEV_TX_OK;
}

int icecap_net_poll(struct napi_struct *napi, int budget)
{
	struct icecap_net_priv *priv = container_of(napi, struct icecap_net_priv, napi);

	size_t n;
	struct sk_buff *skb;
	int work_done = 0;
	int notify = false;

	icecap_log_debug("icecap-net: poll");

	for (; work_done < budget; work_done++) {
		if (icecap_ring_buffer_poll_read_packet(&priv->rb, &n) != ICECAP_RING_BUFFER_PACKET) {
			break;
		}
		icecap_log_debug("icecap-net: rx %ld", n);
		skb = dev_alloc_skb(n + 2);
		BUG_ON(!skb);
		skb_reserve(skb, 2);
		icecap_ring_buffer_read_packet(&priv->rb, n, skb_put(skb, n));
		skb->dev = priv->dev;
		skb->protocol = eth_type_trans(skb, priv->dev);
		netif_receive_skb(skb);
		notify = true;
	}

	if (work_done < budget) {
		napi_complete(napi);
		icecap_net_enable_rx_cb(priv);
	}

	if (notify) {
		icecap_ring_buffer_notify_read(&priv->rb);
	}

	return work_done;
}

static irqreturn_t icecap_net_interrupt(int irq, void *dev_id)
{
	struct icecap_net_priv *priv = dev_id;
	icecap_log_debug("icecap-net: interrupt");
	icecap_net_disable_rx_cb(priv);
	if (napi_schedule_prep(&priv->napi))
		__napi_schedule(&priv->napi);
	return IRQ_HANDLED;
}

int icecap_net_open(struct net_device *dev)
{
	int err;
	struct icecap_net_priv *priv = netdev_priv(dev);

	err = request_irq(priv->irq, icecap_net_interrupt, 0, dev->name, priv);
	BUG_ON(err);

	napi_enable(&priv->napi);

	icecap_net_enable_rx_cb(priv);
	icecap_net_disable_tx_cb(priv);

	netif_start_queue(dev);

	return 0;
}

int icecap_net_stop(struct net_device *dev)
{
	struct icecap_net_priv *priv = netdev_priv(dev);

	netif_stop_queue(dev);

	icecap_net_disable_rx_cb(priv);
	icecap_net_disable_tx_cb(priv);

	napi_disable(&priv->napi);

	free_irq(priv->irq, priv);

	return 0;
}

static const struct net_device_ops icecap_net_ops = {
	.ndo_open = icecap_net_open,
	.ndo_stop = icecap_net_stop,
	.ndo_start_xmit = icecap_net_start_xmit,
	.ndo_validate_addr = eth_validate_addr,
	.ndo_features_check = passthru_features_check,
};

static int probe(struct platform_device *dev)
{
	struct net_device *net_dev;
	struct icecap_net_priv *priv;
	const char *addr;
	int irq;
	int n, err;
	u32 mtu;

	printk("icecap-net: probe");

	irq = platform_get_irq(dev, 0);
	BUG_ON(irq < 0);

	addr = of_get_property(dev->dev.of_node, "local-mac-address", &n);
	BUG_ON(!addr);
	BUG_ON(n != ETH_ALEN);

	net_dev = alloc_etherdev(sizeof(struct icecap_net_priv));
	BUG_ON(net_dev == NULL);
	// net_dev->features |= NETIF_F_RXCSUM; // TODO

	priv = netdev_priv(net_dev);
	memset(priv, 0, sizeof(struct icecap_net_priv));
	icecap_ring_buffer_of(dev, &priv->rb);

	priv->irq = irq;

	priv->dev = net_dev;
	spin_lock_init(&priv->lock);

	net_dev->netdev_ops = &icecap_net_ops;

	err = of_property_read_u32(dev->dev.of_node, "mtu", &mtu);
	BUG_ON(err);
	net_dev->mtu = (unsigned int)mtu;

	memcpy(net_dev->dev_addr, addr, ETH_ALEN);

	netif_napi_add(net_dev, &priv->napi, icecap_net_poll, napi_weight);

	err = register_netdev(net_dev);
	BUG_ON(err);

	return 0;
}

static int remove(struct platform_device *dev)
{
	printk("icecap-net: remove");
	return 0;
}

static struct of_device_id match_table[] = {
	{
		.compatible = "icecap,net",
	},
	{},
};
MODULE_DEVICE_TABLE(of, match_table);

static struct platform_driver icecap_net = {
	.probe = probe,
	.remove = remove,
	.driver = {
		.name = "icecap_net",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(match_table),
	},
};
module_platform_driver(icecap_net);

// TODO
// - Synchronize
// - Checksums unecessary
//	skb->ip_summed = CHECKSUM_UNNECESSARY; ?
//	net_dev->features |= NETIF_F_NO_CSUM; (removed) ?
//	net_dev->features |= NETIF_F_HW_CSUM | NETIF_F_SG | NETIF_F_RXCSUM; ?
//	net_dev->hw_features |= NETIF_F_HW_CSUM | NETIF_F_SG; ?
