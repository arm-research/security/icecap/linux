#define DEBUG

#include <linux/delay.h>
#include <linux/err.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/list.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>
#include <linux/serial_core.h>
#include <linux/stringify.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <uapi/linux/icecap.h>

#include "sel4.h"

#define ICECAP_RESOURCE_SERVER_DEV_MAX (1)

// HACK
#define ICECAP_RESOURCE_SERVER_MAX_NUM_NODES 3

// HACK
uint64_t icecap_resource_server_endpoints[ICECAP_RESOURCE_SERVER_MAX_NUM_NODES];

static dev_t icecap_resource_server_major;

static struct class resource_server_class = {
	.owner = THIS_MODULE,
	.name = "icecap_resource_server",
};

struct resource_server_dev {
	struct device dev;
	struct cdev cdev;
	char *bulk_region;
	size_t bulk_region_size;
	bool open;
};

static int resource_server_open(struct inode *inode, struct file *filp)
{
	struct resource_server_dev *resource_server = container_of(inode->i_cdev, struct resource_server_dev, cdev);
	if (resource_server->open)
		return -EBUSY;
	resource_server->open = true;
	get_device(&resource_server->dev);
	filp->private_data = resource_server;
	return 0;
}

static int resource_server_release(struct inode *inode, struct file *filp)
{
	struct resource_server_dev *resource_server = filp->private_data;
	BUG_ON(!resource_server->open);
	resource_server->open = false;
	put_device(&resource_server->dev);
	return 0;
}

static ssize_t resource_server_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
{
	struct resource_server_dev *resource_server = filp->private_data;

	BUG_ON(count > resource_server->bulk_region_size);
	BUG_ON(copy_from_user(resource_server->bulk_region, buf, count));
	return count;
}

// NOTE see below
static void hack_yield_to(struct icecap_resource_server_yield_to *yield_to);

static long resource_server_unlocked_ioctl(struct file *filp, unsigned int cmd, unsigned long argp)
{
	int ret = 0;

	switch (cmd) {
	case ICECAP_RESOURCE_SERVER_YIELD_TO: {
		struct icecap_resource_server_yield_to yield_to;
		struct icecap_resource_server_yield_to __user *yield_to_user = (struct icecap_resource_server_yield_to __user *)argp;

		if (copy_from_user(&yield_to, (void __user *)yield_to_user, sizeof(yield_to))) {
			return -EFAULT;
		}

		hack_yield_to(&yield_to);

		if (copy_to_user((void __user *)yield_to_user, &yield_to, sizeof(yield_to))) {
			return -EFAULT;
		}
		break;
	}
	default:
		return -EINVAL;
		break;
	}

	return ret;
}

static const struct file_operations resource_server_fops = {
	.owner = THIS_MODULE,
	.open = resource_server_open,
	.release = resource_server_release,
	.write = resource_server_write,
	.unlocked_ioctl = resource_server_unlocked_ioctl
};

static void resource_server_dev_release(struct device *dev)
{
	struct resource_server_dev *resource_server = container_of(dev, struct resource_server_dev, dev);
	kfree(resource_server);
}

static int resource_server_probe(struct platform_device *pdev)
{
	int ret;
	u32 id;
	struct resource *r;
	struct resource_server_dev *resource_server;

	printk("icecap-resource-server: adding");

	id = 0; // TODO

	resource_server = kzalloc(sizeof(*resource_server), GFP_KERNEL);
	if (!resource_server)
		return -ENOMEM;

	r = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	BUG_ON(!r);
	resource_server->bulk_region_size = resource_size(r);
	resource_server->bulk_region = memremap(r->start, resource_size(r), MEMREMAP_WB);
	BUG_ON(!resource_server->bulk_region);

	ret = of_property_read_u64_array(pdev->dev.of_node, "endpoints", &icecap_resource_server_endpoints[0], num_online_cpus());
	BUG_ON(ret);

	device_initialize(&resource_server->dev);
	resource_server->dev.parent = &pdev->dev;
	resource_server->dev.release = resource_server_dev_release;

	cdev_init(&resource_server->cdev, &resource_server_fops);
	resource_server->cdev.owner = THIS_MODULE;

	dev_set_name(&resource_server->dev, "%s", "icecap_resource_server");
	resource_server->dev.id = id;
	resource_server->dev.class = &resource_server_class;
	resource_server->dev.devt = MKDEV(MAJOR(icecap_resource_server_major), id);

	ret = cdev_device_add(&resource_server->cdev, &resource_server->dev);
	if (ret) {
		dev_err(&pdev->dev, "failed to add cdev: %d\n", ret);
		goto put_device;
	}

	dev_set_drvdata(&pdev->dev, resource_server);

	printk("icecap-resource-server: added");
	return 0;

// remove_cdev: // unused for now
	cdev_device_del(&resource_server->cdev, &resource_server->dev);
put_device:
	put_device(&resource_server->dev);
	return ret;
}

static int resource_server_remove(struct platform_device *pdev)
{
	struct resource_server_dev *resource_server = dev_get_drvdata(&pdev->dev);

	cdev_device_del(&resource_server->cdev, &resource_server->dev);
	put_device(&resource_server->dev);

	return 0;
}

static struct of_device_id match_table[] = {
	{
		.compatible = "icecap,resource-server",
	},
	{},
};
MODULE_DEVICE_TABLE(of, match_table);

static struct platform_driver icecap_resource_server_driver = {
	.probe = resource_server_probe,
	.remove = resource_server_remove,
	.driver = {
		.name = "icecap_resource_server",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(match_table),
	},
};

static int __init mod_init(void)
{
	int ret;

	printk("icecap-resource-server: ICECAP_RESOURCE_SERVER_YIELD_TO = 0x%lx", ICECAP_RESOURCE_SERVER_YIELD_TO);

	ret = class_register(&resource_server_class);
	if (ret)
		return ret;

	ret = alloc_chrdev_region(&icecap_resource_server_major, 0, ICECAP_RESOURCE_SERVER_DEV_MAX, "icecap_resource_server");
	if (ret < 0) {
		pr_err("icecap_resource_server: failed to allocate char dev region\n");
		goto unregister_class;
	}

	ret = platform_driver_register(&icecap_resource_server_driver);
	if (ret < 0) {
		pr_err("icecap_resource_server: failed to register driver\n");
		goto unregister_chrdev;
	}

	return 0;

unregister_chrdev:
	unregister_chrdev_region(icecap_resource_server_major, ICECAP_RESOURCE_SERVER_DEV_MAX);
unregister_class:
	class_unregister(&resource_server_class);
	return ret;
}
module_init(mod_init);

static void __exit mod_exit(void)
{
	platform_driver_unregister(&icecap_resource_server_driver);
	unregister_chrdev_region(icecap_resource_server_major, ICECAP_RESOURCE_SERVER_DEV_MAX);
	class_unregister(&resource_server_class);
}
module_exit(mod_exit);


// NOTE here be hacks

#define CNTV_CTL_EL0_ISTATUS (1 << 2)
#define CNTV_CTL_EL0_IMASK (1 << 1)
#define CNTV_CTL_EL0_ENABLE (1 << 0)

#define RESUME_HOST_CONDITION_TAG_TIMEOUT 1
#define RESUME_HOST_CONDITION_TAG_HOST_EVENT 2

static void hack_yield_to(struct icecap_resource_server_yield_to *yield_to)
{
	seL4_MessageInfo_t req;
	seL4_MessageInfo_t resp;
	uint64_t mr0;
	uint64_t mr1;

	uint64_t swap;
	uint64_t optional_timeout;

	u32 cntv_ctl;
	u32 cntfrq;
	u32 cntv_tval;
	u64 timeout;

	int this_cpu;
	int ret = 1;

	while (ret > 0) {

		this_cpu = get_cpu();

		preempt_disable();
		local_irq_disable();

		if (signal_pending(current)) {
			ret = 0;
			goto exit;
		}

		swap = 0;
		swap |= this_cpu << (0 * 16);
		swap |= yield_to->realm_id << (1 * 16);
		swap |= yield_to->virtual_node << (2 * 16);

		cntv_ctl = read_sysreg(cntv_ctl_el0);

		if (cntv_ctl & CNTV_CTL_EL0_ISTATUS) {
			// TODO this shouldn't be happening so often
			ret = 0;
			goto exit;
		}

		if ((cntv_ctl & CNTV_CTL_EL0_ENABLE) && !(cntv_ctl & CNTV_CTL_EL0_IMASK)) {
			cntfrq = read_sysreg(cntfrq_el0);
			cntv_tval = read_sysreg(cntv_tval_el0);
			if (cntv_tval & (1 << 31)) {
				// TODO this shouldn't be happening
				ret = 0;
				goto exit;
			}
			timeout = (((u64)cntv_tval * (1000 * 1000 * 1000)) / ((u64)cntfrq));
			optional_timeout = timeout | ((u64)1 << 63);
		} else {
			optional_timeout = 0;
		}

		mr0 = swap;
		mr1 = optional_timeout;
		req = seL4_MessageInfo_new(0, 0, 0, 2);
		resp = seL4_CallWithMRs(icecap_resource_server_endpoints[this_cpu], req, &mr0, &mr1, 0, 0);

		switch (mr0) {
			case RESUME_HOST_CONDITION_TAG_TIMEOUT:
				ret = 1;
				break;
			case RESUME_HOST_CONDITION_TAG_HOST_EVENT:
				ret = 1;
				break;
			default:
				BUG();
		}

	exit:
		put_cpu();

		local_irq_enable();
		preempt_enable();
	}

	return;
}
