#pragma once

typedef enum {
	icecap_vmm_sys_id_putchar = 1337,
} icecap_vmm_sys_id;

static inline void icecap_vmm_sys_putchar(char c)
{
	register unsigned long int arg asm("x0") = c;
	register unsigned long int sys_id asm("x7") = icecap_vmm_sys_id_putchar;
	asm volatile(
		"hvc #0"
		: : "r"(arg), "r"(sys_id)
	);
}
