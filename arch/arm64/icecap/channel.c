#define DEBUG

#include <linux/delay.h>
#include <linux/err.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/list.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>
#include <linux/serial_core.h>
#include <linux/stringify.h>
#include <linux/types.h>

#include "ring_buffer.h"

/* NOTE see rmtfs_mem.c for simple example of most of what's needed in this module */

#define ICECAP_RING_BUFFER_DEV_MAX (MINORMASK + 1)

static dev_t icecap_ring_buffer_major;

static struct class rb_class = {
	.owner = THIS_MODULE,
	.name = "icecap_channel",
};

struct rb_dev {
	struct device dev;
	struct cdev cdev;
	icecap_ring_buffer_t rb;
	int irq;
	bool open;
	wait_queue_head_t wq;
};

static int rb_open(struct inode *inode, struct file *filp)
{
	struct rb_dev *rrb = container_of(inode->i_cdev, struct rb_dev, cdev);
	if (rrb->open)
		return -EBUSY;
	rrb->open = true;
	get_device(&rrb->dev);
	filp->private_data = rrb;
	return 0;
}

static int rb_release(struct inode *inode, struct file *filp)
{
	struct rb_dev *rrb = filp->private_data;
	BUG_ON(!rrb->open);
	rrb->open = false;
	put_device(&rrb->dev);
	return 0;
}

static ssize_t rb_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	struct rb_dev *rrb = filp->private_data;

	if (wait_event_interruptible(rrb->wq, icecap_ring_buffer_poll_read(&rrb->rb) > 0))
		return -ERESTARTSYS;

	count = min(count, icecap_ring_buffer_poll_read(&rrb->rb));
	icecap_ring_buffer_read_to_user(&rrb->rb, count, buf);
	icecap_ring_buffer_notify_read(&rrb->rb);

	*f_pos += count;
	return count;
}

static ssize_t rb_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
{
	struct rb_dev *rrb = filp->private_data;

	if (wait_event_interruptible(rrb->wq, icecap_ring_buffer_poll_write(&rrb->rb) > 0))
		return -ERESTARTSYS;

	count = min(count, icecap_ring_buffer_poll_write(&rrb->rb));
	icecap_ring_buffer_write_from_user(&rrb->rb, count, buf);
	icecap_ring_buffer_notify_write(&rrb->rb);

	*f_pos += count;
	return count;
}

static const struct file_operations rb_fops = {
	.owner = THIS_MODULE,
	.open = rb_open,
	.release = rb_release,
	.read = rb_read,
	.write = rb_write,
};

static irqreturn_t rb_interrupt(int irq, void *dev_id)
{
	struct rb_dev *rrb = dev_id;
	wake_up_interruptible(&rrb->wq);
	icecap_ring_buffer_enable_notify_read(&rrb->rb);
	icecap_ring_buffer_enable_notify_write(&rrb->rb);
	return IRQ_HANDLED;
}

static void rb_dev_release(struct device *dev)
{
	struct rb_dev *rrb = container_of(dev, struct rb_dev, dev);
	kfree(rrb);
}

static int rb_probe(struct platform_device *pdev)
{
	int ret;
	u32 id;
	const char *name;
	struct rb_dev *rrb;

	rrb = kzalloc(sizeof(*rrb), GFP_KERNEL);
	if (!rrb)
		return -ENOMEM;

	ret = of_property_read_string(pdev->dev.of_node, "channel-name", &name);
	if (ret) {
		dev_err(&pdev->dev, "failed to parse \"name\"\n");
		return ret;
	}

	ret = of_property_read_u32(pdev->dev.of_node, "id", &id);
	if (ret) {
		dev_err(&pdev->dev, "failed to parse \"id\"\n");
		return ret;
	}

	rrb->irq = platform_get_irq(pdev, 0);
	if (ret) {
		dev_err(&pdev->dev, "failed to get irq\n");
		return ret;
	}

	init_waitqueue_head(&rrb->wq);

	icecap_ring_buffer_of(pdev, &rrb->rb);
	icecap_ring_buffer_enable_notify_read(&rrb->rb);
	icecap_ring_buffer_enable_notify_write(&rrb->rb);

	device_initialize(&rrb->dev);
	rrb->dev.parent = &pdev->dev;
	rrb->dev.release = rb_dev_release;

	cdev_init(&rrb->cdev, &rb_fops);
	rrb->cdev.owner = THIS_MODULE;

	dev_set_name(&rrb->dev, "%s", name);
	rrb->dev.id = id;
	rrb->dev.class = &rb_class;
	rrb->dev.devt = MKDEV(MAJOR(icecap_ring_buffer_major), id);

	ret = cdev_device_add(&rrb->cdev, &rrb->dev);
	if (ret) {
		dev_err(&pdev->dev, "failed to add cdev: %d\n", ret);
		goto put_device;
	}

	dev_set_drvdata(&pdev->dev, rrb);

	ret = request_irq(rrb->irq, rb_interrupt, 0, dev_name(&rrb->dev), rrb);
	if (ret) {
		dev_err(&pdev->dev, "failed to request irq: %d\n", ret);
		goto remove_cdev;
	}

	return 0;

remove_cdev:
	cdev_device_del(&rrb->cdev, &rrb->dev);
put_device:
	put_device(&rrb->dev);
	return ret;
}

static int rb_remove(struct platform_device *pdev)
{
	struct rb_dev *rrb = dev_get_drvdata(&pdev->dev);

	free_irq(rrb->irq, rrb);
	cdev_device_del(&rrb->cdev, &rrb->dev);
	put_device(&rrb->dev);

	return 0;
}

static struct of_device_id match_table[] = {
	{
		.compatible = "icecap,channel",
	},
	{},
};
MODULE_DEVICE_TABLE(of, match_table);

static struct platform_driver icecap_ring_buffer_driver = {
	.probe = rb_probe,
	.remove = rb_remove,
	.driver = {
		.name = "icecap_channel",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(match_table),
	},
};

static int __init mod_init(void)
{
	int ret;

	ret = class_register(&rb_class);
	if (ret)
		return ret;

	ret = alloc_chrdev_region(&icecap_ring_buffer_major, 0, ICECAP_RING_BUFFER_DEV_MAX, "icecap_channel");
	if (ret < 0) {
		pr_err("icecap_ring_buffer: failed to allocate char dev region\n");
		goto unregister_class;
	}

	ret = platform_driver_register(&icecap_ring_buffer_driver);
	if (ret < 0) {
		pr_err("icecap_ring_buffer: failed to register driver\n");
		goto unregister_chrdev;
	}

	return 0;

unregister_chrdev:
	unregister_chrdev_region(icecap_ring_buffer_major, ICECAP_RING_BUFFER_DEV_MAX);
unregister_class:
	class_unregister(&rb_class);
	return ret;
}
module_init(mod_init);

static void __exit mod_exit(void)
{
	platform_driver_unregister(&icecap_ring_buffer_driver);
	unregister_chrdev_region(icecap_ring_buffer_major, ICECAP_RING_BUFFER_DEV_MAX);
	class_unregister(&rb_class);
}
module_exit(mod_exit);
