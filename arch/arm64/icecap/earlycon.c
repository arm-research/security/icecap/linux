#include <linux/serial_core.h>

#include "sel4.h"
#include "vmm.h"

static void icecap_earlycon_sel4_write(struct console *console, const char *buf, unsigned n)
{
	const char *end = buf + n;
	while (buf < end) {
		seL4_DebugPutChar(*buf++);
	}
}

static int __init icecap_earlycon_sel4_setup(struct earlycon_device *device, const char *opt)
{
	device->con->write = icecap_earlycon_sel4_write;
	return 0;
}
EARLYCON_DECLARE(icecap_sel4, icecap_earlycon_sel4_setup);

static void icecap_earlycon_vmm_write(struct console *console, const char *buf, unsigned n)
{
	const char *end = buf + n;
	while (buf < end) {
		icecap_vmm_sys_putchar(*buf++);
	}
}

static int __init icecap_earlycon_vmm_setup(struct earlycon_device *device, const char *opt)
{
	device->con->write = icecap_earlycon_vmm_write;
	return 0;
}
EARLYCON_DECLARE(icecap_vmm, icecap_earlycon_vmm_setup);
